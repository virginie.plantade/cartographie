function loadData(adress, callback) {
    document.getElementById('loading').classList.remove('hide');
    document.getElementById('checkboxesWrapper').classList.add('hide');
  
    fetch(adress)
      .then(response => {
        return response.json()
      })
      .then(data => {
  
        // Work with JSON data here
        callback(data)
        document.getElementById('loading').classList.add('hide');
        document.getElementById('checkboxesWrapper').classList.remove('hide');
  
      })
      .catch(e => {
        console.log(e);
      });
  }
  export {loadData}